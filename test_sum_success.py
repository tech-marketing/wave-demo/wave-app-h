from functions import sum 

def test_sum_1():
    assert(sum(num_1=0, num_2=0)==0)

def test_sum_2():
    assert(sum(num_1=0, num_2=1)==1)

def test_sum_3():
    assert(sum(num_1=1, num_2=0)==1)

def test_sum_3():
    assert(sum(num_1=-1, num_2=0)==-1)

