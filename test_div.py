from functions import div

def test_div_1():
    assert(div(num_1=0, num_2=2)==0)

def test_div_2():
    assert(div(num_1=2, num_2=2)==1)

def test_div_3():
    assert(div(num_1=2, num_2=0)=="ZeroDivisionError")



